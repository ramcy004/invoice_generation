<?php 
session_start();
include('inc/header.php');
include 'Invoice.php';
$invoice = new Invoice();
$invoice->checkLoggedIn();
?>

<body class="">
  
  <div class="container" style="min-height:500px;">
  <div class=''>
  </div>
	<div class="container" style="background: rgb(92 25 25 / 2%);
    border: 1px solid #2a3e3426;
    margin-top: 6%;">		
	  <h2 class="title" style="text-align:center">Invoice / List</h2>
    <hr>
    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" >
				
				</div>
		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
					<span style="float:right">
					<?php include('menu.php');?>
</span>	
				</div>			  
      <table id="data-table" class="table table-bordered table-condensed table-striped">
        <thead>
          <tr>
            <th>Invoice No.</th>
            <th>Create Date</th>
            <th>Customer Name</th>
            <th>Total Amount</th>
            <th>Generate</th>
            
          </tr>
        </thead>
        <?php		
		$invoiceList = $invoice->getInvoiceList();
        foreach($invoiceList as $invoiceDetails){
			$invoiceDate = date("d/m/Y", strtotime($invoiceDetails["order_date"]));
            echo '
              <tr>
                <td>'.$invoiceDetails["order_id"].'</td>
                <td>'.$invoiceDate.'</td>
                <td>'.$invoiceDetails["order_receiver_name"].'</td>
                <td>'.$invoiceDetails["order_total_after_tax"].'</td>
                <td><a href="print_invoice.php?invoice_id='.$invoiceDetails["order_id"].'" title="Print Invoice">Generate Invoice</a></td>
               
              </tr>
            ';
        }       
        ?>
      </table>	
</div>	
<?php include('inc/footer.php');?>